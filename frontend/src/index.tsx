import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {QueryCache, ReactQueryCacheProvider} from "react-query";

const queryCache = new QueryCache();

ReactDOM.render(<>
    <ReactQueryCacheProvider queryCache={queryCache}>
        <App />
    </ReactQueryCacheProvider>
    </>, document.getElementById('root')
);
