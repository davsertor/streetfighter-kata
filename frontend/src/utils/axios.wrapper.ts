import axios, {AxiosInstance, AxiosResponse} from 'axios';

export class AxiosWrapper {
  private readonly instance: AxiosInstance;

  constructor() {
      // const baseUrl = (window as any).API_BASE_URL;
      this.instance = axios.create({
          withCredentials: true,
          // baseURL: baseUrl ?? '/'
      });
  }

  async get<T>(path: string, config?: any): Promise<AxiosResponse<T>> {
    return await this.instance.get<T>(path, config);
  }

  async post(path: string, payload: any = {}) {
    return await this.instance.post(path, payload);
  }

  async put(path: string, payload?: any) {
    return this.instance.put(path, payload);
  }

  async delete(path: string) {
    return this.instance.delete(path);
  }
}
