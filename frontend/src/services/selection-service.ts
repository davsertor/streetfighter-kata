import {Position} from "../domain/classes";
import {Grid} from "../../../_frontend/src/domain/classes";

export class SelectionService {
    static async fetchFighters() {
        const response = await fetch("http://localhost:8080/selection");
        const json = await response.json();

        return json as Grid;
    }

    static async move(fromPosition: Position, movement: string) {
        const response = await fetch(`http://localhost:8080/selection/${fromPosition.row}/${fromPosition.column}/move/${movement}`);
        const json = await response.json();

        return json as Grid;
    }
}