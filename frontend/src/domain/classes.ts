export interface GridDTO {
    rows: Array<Array<CellDTO>>;
    currentPosition: Position;
}

export const emptyGrid: GridDTO = {
  rows: [],
  currentPosition: { row: 0, column: 0 }
};

export interface CellDTO {
    position: Position;
    fighter: Fighter;
    selected: boolean;
}

export interface Position {
    row: number;
    column: number;
}

export interface Fighter {
    name: string;
}