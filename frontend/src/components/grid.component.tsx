import React from 'react';
import {CellDTO, GridDTO} from '../domain/classes';

export const Grid = ({ grid }: { grid: GridDTO }) => {
  return <>
      <div className='flex flex-col'>
          {grid.rows.map((row, rowIndex) => <div className='flex' key={rowIndex}>
              {row.map((cell, cellIndex) => <Cell cell={cell} key={cellIndex}/>)}
          </div>)}
      </div>
  </>
};

export const Cell = ({ cell }: { cell: CellDTO }) => {
    return <div className={`flex flex-col justify-center w-24 h-48 border border-1 border-solid ${cell.selected && 'bg-blue-500 text-white'}`}>
        {cell.fighter.name}
    </div>;
};

export const Arrows = ({onUp, onDown, onLeft, onRight}: {onUp: () => void, onDown: () => void, onLeft: () => void, onRight: () => void}) => {
    return <>
        <div className='flex flex-col w-auto h-24'>
            <div className='flex h-full'>
                <div className='w-24'>

                </div>
                <div className='w-24'>
                    <button className='w-full h-full bg-gray-600 border border-1 border-solid p-2' onClick={onUp}>up</button>
                </div>
                <div className='w-24'>

                </div>
            </div>
            <div className='flex h-full'>
                <div className='w-24'>
                    <button className='w-full h-full bg-gray-600 border border-1 border-solid p-2' onClick={onLeft}>left</button>
                </div>
                <div className='w-24'>
                    <button className='w-full h-full bg-gray-600 border border-1 border-solid p-2' onClick={onDown}>down</button>
                </div>
                <div className='w-24'>
                    <button className='w-full h-full bg-gray-600 border border-1 border-solid p-2' onClick={onRight}>right</button>
                </div>
            </div>
        </div>
    </>;
};