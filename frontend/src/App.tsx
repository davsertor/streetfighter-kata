import React, {useEffect, useState} from 'react';
import {SelectionService} from "./services/selection-service";
import {Arrows, Grid} from "./components/grid.component";
import {useMutation, useQuery} from "react-query";
import {emptyGrid, Position} from "./domain/classes";

export const useFetchFighters = () => {
  return useQuery(
      ['fighters'],
      () => SelectionService.fetchFighters()
  );
}

export const useMove = () => {
  return useMutation(
      ({ currentPosition, movement }: { currentPosition: Position, movement: string }) => SelectionService.move(currentPosition, movement)
  );
}

const App: React.FC = () => {
  const {isLoading, data: initialGrid} = useFetchFighters();
  const [move, {isLoading: moveIsLoading, data: movedGrid}] = useMove();
  const [grid, setGrid] = useState(emptyGrid);

  useEffect(() => {
    if (initialGrid) setGrid(initialGrid);
  }, [initialGrid]);

  useEffect(() => {
    if (movedGrid) setGrid(movedGrid);
  }, [movedGrid]);

  if (isLoading) {
    return <>Loading</>;
  }

  return (
    <div className="m-auto antialiased font-sans font-serif font-mono text-center">
    <header className="bg-gray-900 min-h-screen flex flex-col items-center justify-center text-white text-sm">
      <Grid grid={grid}/>

      <br/>

      <Arrows
          onUp={() => move({ currentPosition: grid.currentPosition, movement: 'up' })}
          onDown={() => move({ currentPosition: grid.currentPosition, movement: 'down' })}
          onLeft={() => move({ currentPosition: grid.currentPosition, movement: 'left' })}
          onRight={() => move({ currentPosition: grid.currentPosition, movement: 'right' })}
      />
    </header>
  </div>
  );
}

export default App;
