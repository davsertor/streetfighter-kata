package com.streetfighter.test.domain

import com.streetfighter.domain.Cell
import com.streetfighter.domain.Fighter
import com.streetfighter.domain.Position
import com.streetfighter.domain.directions.DownDirection
import org.junit.jupiter.api.Test

class DownDirectionUT {
    private val one = Cell(Position(0, 0), Fighter("one"))
    private val two = Cell(Position(0, 1), Fighter("two"))
    private val three = Cell(Position(0, 4), Fighter("three"))

    @Test
    fun should_get_next() {
        val sut = DownDirection(
            listOf(
                one,
                two,
                Cell(Position(0, 2), Fighter("")),
                Cell(Position(0, 3), Fighter("")),
                three
            ),
            one
        )
        assert(sut.next() == two)
    }

    @Test
    fun empty_next_should_get_same() {
        val sut = DownDirection(
            listOf(
                one,
                two,
                Cell(Position(0, 2), Fighter("")),
                Cell(Position(0, 3), Fighter("")),
                three
            ),
            two
        )
        assert(sut.next() == two)
    }

    @Test
    fun last_next_should_get_same() {
        val sut = DownDirection(
            listOf(
                one,
                two,
                Cell(Position(0, 2), Fighter("")),
                Cell(Position(0, 3), Fighter("")),
                three
            ),
            three
        )
        assert(sut.next() == three)
    }
}
