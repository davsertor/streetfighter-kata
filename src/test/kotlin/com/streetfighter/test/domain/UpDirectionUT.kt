package com.streetfighter.test.domain

import com.streetfighter.domain.Cell
import com.streetfighter.domain.Fighter
import com.streetfighter.domain.Position
import com.streetfighter.domain.directions.UpDirection
import org.junit.jupiter.api.Test

class UpDirectionUT {
    private val one = Cell(Position(0, 0), Fighter("one"))
    private val two = Cell(Position(0, 1), Fighter("two"))
    private val three = Cell(Position(0, 4), Fighter("three"))

    @Test
    fun should_get_previous() {
        val sut = UpDirection(
            listOf(
                one,
                two,
                Cell(Position(0, 2), Fighter("")),
                Cell(Position(0, 3), Fighter("")),
                three
            ),
            two
        )
        assert(sut.next() == one)
    }

    @Test
    fun empty_previous_should_get_same() {
        val sut = UpDirection(
            listOf(
                one,
                two,
                Cell(Position(0, 2), Fighter("")),
                Cell(Position(0, 3), Fighter("")),
                three
            ),
            three
        )
        assert(sut.next() == three)
    }

    @Test
    fun first_previous_should_get_same() {
        val sut = UpDirection(
            listOf(
                one,
                two,
                Cell(Position(0, 2), Fighter("")),
                Cell(Position(0, 3), Fighter("")),
                three
            ),
            one
        )
        assert(sut.next() == one)
    }
}
