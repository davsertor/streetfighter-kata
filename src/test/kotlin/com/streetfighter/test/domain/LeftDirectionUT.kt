package com.streetfighter.test.domain

import com.streetfighter.domain.Cell
import com.streetfighter.domain.Fighter
import com.streetfighter.domain.Position
import com.streetfighter.domain.directions.LeftDirection
import org.junit.jupiter.api.Test

class LeftDirectionUT {
    private val one = Cell(Position(0, 1), Fighter("one"))
    private val two = Cell(Position(0, 2), Fighter("two"))
    private val three = Cell(Position(0, 5), Fighter("three"))

    @Test
    fun should_get_previous() {
        val sut = LeftDirection(
            listOf(
                one,
                two,
                Cell(Position(0, 2), Fighter("")),
                Cell(Position(0, 3), Fighter("")),
                three
            ),
            two
        )
        assert(sut.next() == one)
    }

    @Test
    fun should_ignore_empties_and_get_previous() {
        val sut = LeftDirection(
            listOf(
                one,
                two,
                Cell(Position(0, 2), Fighter("")),
                Cell(Position(0, 3), Fighter("")),
                three
            ),
            three
        )
        assert(sut.next() == two)
    }

    @Test
    fun should_rotate_to_last_and_get_previous() {
        val sut = LeftDirection(
            listOf(
                one,
                two,
                Cell(Position(0, 2), Fighter("")),
                Cell(Position(0, 3), Fighter("")),
                three
            ),
            one
        )
        assert(sut.next() == three)
    }
}
