package com.streetfighter

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class StreetFighterApplication

fun main(args: Array<String>) {
    runApplication<StreetFighterApplication>(*args)
}
