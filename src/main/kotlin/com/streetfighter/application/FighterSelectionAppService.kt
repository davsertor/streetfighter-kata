package com.streetfighter.application

import com.streetfighter.domain.*
import org.springframework.stereotype.Service

data class GridDTO(val rows: List<List<CellDTO>>, val currentPosition: Position)
data class CellDTO(val position: Position, val fighter: Fighter, val selected: Boolean = false)

@Service
class FighterSelectionAppService {

    fun getFighters(): GridDTO {
        val initialPosition = Position(0, 1)
        val rows = Grid.default(initialPosition).cells
            .map { CellDTO(it.position, it.fighter, it.position == Position(0, 1)) }
            .groupBy { it.position.row }
            .map { it.value }

        return GridDTO(rows, initialPosition)
    }

    fun move(fromPosition: Position, movement: Movement): GridDTO {
        val grid = Grid.default(fromPosition).move(movement)
        val rows = grid.cells
            .map { CellDTO(it.position, it.fighter, it.position == grid.currentCell.position) }
            .groupBy { it.position.row }
            .map { it.value }

        return GridDTO(rows, grid.currentCell.position)
    }


}