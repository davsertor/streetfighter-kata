package com.streetfighter.domain.directions

import com.streetfighter.domain.Cell
import com.streetfighter.domain.Direction

data class UpDirection(val cells: List<Cell>, val from: Cell): Direction {
    private val fromIndexOf = cells.indexOf(from)
    private val previousIndexOf = fromIndexOf - 1

    override fun next(): Cell {
        if (fromIndexOf == 0)
            return from

        if (cells[previousIndexOf].isEmpty())
            return from

        return cells[previousIndexOf]
    }
}