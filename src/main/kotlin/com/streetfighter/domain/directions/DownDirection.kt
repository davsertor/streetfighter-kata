package com.streetfighter.domain.directions

import com.streetfighter.domain.Cell
import com.streetfighter.domain.Direction

data class DownDirection(val cells: List<Cell>, val from: Cell): Direction {
    private val fromIndexOf = cells.indexOf(from)
    private val nextIndexOf = fromIndexOf + 1

    override fun next(): Cell {
        if (fromIndexOf >= cells.lastIndex)
            return from

        if (cells[nextIndexOf].isEmpty())
            return from

        return cells[nextIndexOf]
    }
}