package com.streetfighter.domain.directions

import com.streetfighter.domain.Cell
import com.streetfighter.domain.Direction

data class RightDirection(val cells: List<Cell>, val from: Cell): Direction {
    private val fromIndexOf = cells.indexOf(from)
    private val nextIndexOf = fromIndexOf + 1

    override fun next(): Cell {
        val reorderedItems = cells.takeLast(cells.size - nextIndexOf) + cells.take(fromIndexOf)
        val cleanReorderedItems = reorderedItems.filter { it.isNotEmpty() }

        return cleanReorderedItems.first()
    }
}