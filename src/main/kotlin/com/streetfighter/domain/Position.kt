package com.streetfighter.domain

data class Position(val row: Int, val column: Int) {
    companion object {
        fun create(position: IntArray): Position = Position(position[0], position[1])
    }
}