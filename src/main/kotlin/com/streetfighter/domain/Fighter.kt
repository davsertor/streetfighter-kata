package com.streetfighter.domain

data class Fighter(val name: String)