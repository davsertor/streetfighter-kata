package com.streetfighter.domain

interface Direction {
    fun next(): Cell
}