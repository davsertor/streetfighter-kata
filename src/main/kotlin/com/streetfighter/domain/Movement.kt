package com.streetfighter.domain

enum class Movement {
    UP {
        override fun move(grid: Grid) = grid.up()
    },
    DOWN {
        override fun move(grid: Grid) = grid.down()
    },
    LEFT {
        override fun move(grid: Grid) = grid.left()
    },
    RIGHT {
        override fun move(grid: Grid) = grid.right()
    };

    abstract fun move(grid: Grid): Grid
}

fun String.asMovement() = Movement.valueOf(this.toUpperCase())