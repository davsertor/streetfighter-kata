package com.streetfighter.domain

import com.streetfighter.domain.directions.DownDirection
import com.streetfighter.domain.directions.UpDirection
import com.streetfighter.domain.directions.LeftDirection
import com.streetfighter.domain.directions.RightDirection

data class Cell(val position: Position, val fighter: Fighter) {
    fun row() = position.row
    fun column() = position.column
    fun isEmpty() = fighter.name.isEmpty()
    fun isNotEmpty() = !isEmpty()
}

fun List<Cell>.rowOf(cell: Cell) = this.filter { it.row() == cell.row() }
fun List<Cell>.columnOf(cell: Cell) = this.filter { it.column() == cell.column() }

fun List<Cell>.upFrom(cell: Cell) = UpDirection(this, cell).next()
fun List<Cell>.downFrom(cell: Cell) = DownDirection(this, cell).next()
fun List<Cell>.leftFrom(cell: Cell) = LeftDirection(this, cell).next()
fun List<Cell>.rightFrom(cell: Cell) = RightDirection(this, cell).next()