package com.streetfighter.domain

data class Grid(val cells: List<Cell>, val currentCell: Cell, val history: List<Cell> = emptyList()) {
    fun move(movement: Movement): Grid =
        movement.move(this)

    fun up(): Grid {
        val nextCell = cells
            .columnOf(currentCell)
            .upFrom(currentCell)

        return copy(currentCell = nextCell, history = history + nextCell)
    }

    fun down(): Grid {
        val nextCell = cells
            .columnOf(currentCell)
            .downFrom(currentCell)

        return copy(currentCell = nextCell, history = history + nextCell)
    }

    fun left(): Grid {
        val nextCell = cells
            .rowOf(currentCell)
            .leftFrom(currentCell)

        return copy(currentCell = nextCell, history = history + nextCell)
    }

    fun right(): Grid {
        val nextCell = cells
            .rowOf(currentCell)
            .rightFrom(currentCell)

        return copy(currentCell = nextCell, history = history + nextCell)
    }

    companion object {
        fun default(initialPosition: Position): Grid {
            val fighters = arrayOf(
                arrayOf("", "Ryu", "E.Honda", "Blanka", "Guile", ""),
                arrayOf("Balrog", "Ken", "Chun Li", "Zangief", "Dhalsim", "Sagat"),
                arrayOf("Vega", "T.Hawk", "Fei Long", "Deejay", "Cammy", "M.Bison")
            )

            return create(fighters, initialPosition)
        }

        fun create(fighters: Array<Array<String>>, initialPosition: IntArray): Grid {
            return create(fighters, Position.create(initialPosition))
        }

        fun create(fighters: Array<Array<String>>, initialPosition: Position): Grid {
            val cells = fighters
                .mapIndexed { rowIndex, rows -> rows.mapIndexed { columnIndex, fighter -> Cell(Position(rowIndex, columnIndex), Fighter(fighter)) } }
                .flatten()

            val initialCell = cells
                .find { it.position == initialPosition }

            return Grid(cells, initialCell!!)
        }
    }
}