package com.streetfighter.controllers

import com.streetfighter.application.FighterSelectionAppService
import com.streetfighter.domain.Grid
import com.streetfighter.domain.Position
import com.streetfighter.domain.asMovement
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class FighterSelectionController @Autowired constructor(val figtherSelectionAppService: FighterSelectionAppService) {

    @GetMapping("/selection")
    fun getFighters() =
        figtherSelectionAppService.getFighters()

    @GetMapping("/selection/{row}/{column}/move/{movement}")
    fun retrieveStudentByName(
        @PathVariable row: Int,
        @PathVariable column: Int,
        @PathVariable movement: String
    ) =
        figtherSelectionAppService.move(Position(row, column), movement.asMovement())

}